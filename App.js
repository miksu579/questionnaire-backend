require('dotenv').config()
const express = require("express");
const mysql = require("mysql");
const app = express();

const bodyParser = require("body-parser");
const port = 5000;

var connection = mysql.createConnection({
  //properties..
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: process.env.DB_NAME,
});

const db = {
  getQuestions: async function () {
    let sql = "Select * from question";
    let questions = [];
    console.log(questions);
    questions = await connection.query(sql);
    console.log(questions);
    return questions;
  },
  getAnything: function(){
    connection.query("Select * from question", function(err,result,fields){
      return result;
    });
  }
};

connection.connect(function (error) {
  if (error) {
    console.log("FAILED TO CONNECT TO DB");
    return;
  }
  console.log("connection to db success");
});

app.use(bodyParser.json());
//CORS
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Auhtorization"
  );
  if (req.method === "OPTIONS") {
    res.header("Access-Control-Allow-Methods", "PUT, POST, PATCH, DELETE, GET");
    return res.status(200).json({});
  }
  next();
});

app.get("/questions", (req, res) => {
      connection.query("select * from question", (err,result,fields) =>{
          res.send(result);
      });
  });

app.post('/savequestion', (req,res) =>{
  console.log(req.body);
  let sql = "UPDATE question set name = ? where id = ?";
  connection.query(sql, [req.body.name, req.body.id], function(err,result,fields){
    if(err) throw err;
    console.log("Question saved");
    res.sendStatus(200);
  })
});

app.post('/newquestion', (req,res) =>{
  let sql = "INSERT INTO question (name) VALUES (?);";
  connection.query(sql,[req.body.name], function (err, result, fields){
    if (err) res.sendStatus(501);
    console.log(`New question: ${req.body.name} -- saved!`)
    res.sendStatus(200);
  })
});

app.delete('/deletequestion', (req,res) =>{
  let sql = " DELETE from question where id = ?";
  connection.query(sql, [req.body.id], function (err,result,fields){
    if (err) {
      // res.sendStatus(500);
      // return;
      throw err;
    }
    console.log(`question with id: ${req.body.id} has been succesfully deleted.`);
    res.sendStatus(200);
  })
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
